/*
 Navicat Premium Data Transfer

 Source Server         : dgsa
 Source Server Type    : MySQL
 Source Server Version : 50737
 Source Host           : localhost:3306
 Source Schema         : lifecycle

 Target Server Type    : MySQL
 Target Server Version : 50737
 File Encoding         : 65001

 Date: 01/04/2023 14:44:57
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for lifecycle
-- ----------------------------
DROP TABLE IF EXISTS `lifecycle`;
CREATE TABLE `lifecycle`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shop_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `shop_img` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `price` decimal(10, 2) NULL DEFAULT NULL,
  `shop_num` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of lifecycle
-- ----------------------------
INSERT INTO `lifecycle` VALUES (1, 'scenery', 'https://img2.baidu.com/it/u=2361676761,992841863&fm=253&fmt=auto&app=120&f=JPEG?w=1200&h=800', 100.00, 100);
INSERT INTO `lifecycle` VALUES (2, 'scenery', 'https://img2.baidu.com/it/u=2361676761,992841863&fm=253&fmt=auto&app=120&f=JPEG?w=1200&h=800', 100.00, 100);
INSERT INTO `lifecycle` VALUES (3, 'scenery', 'https://img2.baidu.com/it/u=2361676761,992841863&fm=253&fmt=auto&app=120&f=JPEG?w=1200&h=800', 100.00, 100);

SET FOREIGN_KEY_CHECKS = 1;
