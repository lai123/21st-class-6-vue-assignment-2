let express=require('express');
let app=express();
let mysql=require('mysql');
let conn=mysql.createConnection({host:'localhost',user:'root',password:'root',database:'lifecycle'})
conn.connect();
app.use(express.static(__dirname));
app.get('/data',(req,res)=>{
    let sql='select * from lifecycle';
    conn.query(sql,(err,data)=>{
        let result={
            code:null,
            data:null,
            msg:null,
        }
        if(err){
            result.code=400;
           result.data=err;
           result.msg='请求失败'
           
        }else{
            result.code=200;
            result.data=data;
            result.msg='请求成功'
        }
        res.end(JSON.stringify(result))
    })
})

app.listen(8080)