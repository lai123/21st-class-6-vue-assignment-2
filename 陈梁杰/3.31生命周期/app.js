const express = require('express');

const app = express();
//调用数据库模型
const User = require('./model/userModel');

//静态路由
app.use(express.static(__dirname))

//获取前端传值
app.use(express.urlencoded({extended:false}))


//接口
app.post('/api',(req,res)=>{
     
    User.findAll().then(data=>{
        res.send(data)
    })


})


app.listen(8080,()=>{
     console.log('监听中');
})