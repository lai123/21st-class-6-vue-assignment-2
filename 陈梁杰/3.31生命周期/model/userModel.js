const {Model, DataTypes} = require('sequelize');

const  sequelize= require('./model');


class User extends Model{}


User.init({
  
  userId:{
      type:DataTypes.INTEGER,
      primaryKey:true,
      autoIncrement:true,
  },

  userName:{
      type:DataTypes.STRING,
  },
  
  password:{
     type:DataTypes.STRING,
  },

  classId:{
    type:DataTypes.INTEGER,
 },


},{
    sequelize,
    modelName:'User',
    tableName:'user2'   
})



module.exports=User