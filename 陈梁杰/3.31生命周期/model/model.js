const {Sequelize} = require('sequelize');

const {dbName,host,user,password} = require('../config/config');


let sequelize = new Sequelize(dbName,user,password,{
       host:host,
       dialect:'mysql',
       define:{
          underscored:true,
       },      
       timezone:'+8:00',

})


module.exports=sequelize